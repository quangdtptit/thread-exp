package com.example.utils.dto;

import com.example.utils.ThreadUtil;

import java.util.Date;
import java.util.concurrent.BlockingQueue;

public class WorkerDTO implements Runnable {

    private String nameThread;
    private BlockingQueue<Integer> blockingQueue;
    private int typeConstructor = 0;
    private boolean isProducer;

    public WorkerDTO(String nameThread) {
        this.nameThread = nameThread;
        this.typeConstructor = 0;
    }

    public WorkerDTO(BlockingQueue<Integer> blockingQueue, boolean isProducer) {
        this.blockingQueue = blockingQueue;
        this.typeConstructor = 1;
        this.isProducer = isProducer;
    }

    @Override
    public void run() {
        if (this.typeConstructor == 0) {
            System.out.println(Thread.currentThread().getName() + " start ... name thread " + this.nameThread + " time " + new Date());
            ThreadUtil.sleep(ThreadUtil.getRandom(1000, 2000));
            System.out.println(Thread.currentThread().getName() + " finish ... name thread " + this.nameThread);
        }
        if (this.typeConstructor == 1) {
            try {
                while (true) {
                    System.out.println("SIZE QUEUE: " + blockingQueue.size());
                    if (this.isProducer) {
                        int value = ThreadUtil.getRandom(1, 100);
                        System.out.println("Producer put " + value);
                        blockingQueue.put(value);
                        ThreadUtil.sleep(ThreadUtil.getRandom(1000, 3000));
                    } else {
                        Integer value = blockingQueue.take();
                        System.out.println("Consumer take " + value);
                        ThreadUtil.sleep(ThreadUtil.getRandom(1000, 3000));
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
