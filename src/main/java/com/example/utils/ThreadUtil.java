package com.example.utils;

import com.example.utils.dto.WorkerDTO;

import java.util.Random;
import java.util.concurrent.*;

public class ThreadUtil {

    public static void sleep(long mSecond) {
        try {
            Thread.sleep(mSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static int getRandom(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }

    // khởi tạo 1 ThreadPool với số lượng thead max là 1
    // runable 1 worker và các worker còn lại ở trong trạng thái waitting
    // sau khi worker đó hoàn thành, 1 worker khác sẽ được chọn và tiếp tục
    public static void singleThread() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        for (int i = 0; i < 20; i++) {
            WorkerDTO workerDTO = new WorkerDTO("NAME " + i);
            executorService.execute(workerDTO);
        }
        executorService.shutdown();
        // Wait until all threads are finish
        while (!executorService.isTerminated()) {
            // Running ...
        }
        System.out.println("Finished all threads");
    }

    // khởi tạo 1 ThreadPool với số lượng thead là n
    // runable n worker còn sumWorker n ở trạng thái waiting
    // bất cứ khi nào 1 worker trong pool hoàn thành, 1 worker khác sẽ được chọn và tiếp tục
    public static void fixedThread() {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 20; i++) {
            WorkerDTO workerDTO = new WorkerDTO("NAME " + i);
            executorService.execute(workerDTO);
        }
        executorService.shutdown();
        // Wait until all threads are finish
        while (!executorService.isTerminated()) {
            // Running ...
        }
        System.out.println("Finished all threads");
    }

    // khởi tạo 1 ThreadPool với số lượng thead là Integer.MAX
    // tuy nhiên cách hoạt động cacheThread là tái sử dụng
    // các worker lần lượt được runable, tuy nhiên giả sử có 1 worker đầu hoàn thành , thì thread đó sẽ được tái sử dụng cho các worker sau
    // cache sẽ tự động tính toán và tìm các thread trong pool đang nhàn rỗi
    public static void cacheThread() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 20; i++) {
            WorkerDTO workerDTO = new WorkerDTO("NAME " + i);
            executorService.execute(workerDTO);
        }
        executorService.shutdown();
        // Wait until all threads are finish
        while (!executorService.isTerminated()) {
            // Running ...
        }
        System.out.println("Finished all threads");
    }

    public static void scheduleThread() {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2);
        for (int i = 0; i < 20; i++) {
            WorkerDTO workerDTO = new WorkerDTO("NAME " + i);
            executorService.scheduleWithFixedDelay(workerDTO, 0, 3, TimeUnit.SECONDS);
        }
//        try {
//            executorService.awaitTermination(10, TimeUnit.SECONDS);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        executorService.shutdown();
//        // Wait until all threads are finish
//        while (!executorService.isTerminated()) {
//            // Running ...
//        }
//        System.out.println("Finished all threads");
    }

    // Blocking queue , 1 queue để phục vụ bọn luồng theo kiểu producer và consumer , chả khác gì kafka
    // tạo 1 blocking queue có n phần tử thực hiện theo cơ chế FIFO
    // nếu có n producer(thread) đẩy value vào blocking queue, nếu queue đầy thì thread đó sẽ bị wait
    // có n luồng consumer lấy dữ liệu từ queue ra , nếu queue = 0 thì thread đó sẽ bị wait
    public static void useBlockingQueue() {
        BlockingQueue<Integer> blockingQueue = new ArrayBlockingQueue<>(10);
        WorkerDTO producer = new WorkerDTO(blockingQueue, true);
        WorkerDTO consumer = new WorkerDTO(blockingQueue, false);

        new Thread(producer).start();
        new Thread(producer).start();
        new Thread(consumer).start();
    }

    public static void main(String[] args) {
        useBlockingQueue();
    }
}
