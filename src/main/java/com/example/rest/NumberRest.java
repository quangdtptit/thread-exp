package com.example.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class NumberRest {

	@GetMapping("/numbers/{value}")
	public Integer getSquare(@PathVariable Integer value) {
		loading();
		return value * 2;
	}

	@GetMapping("/numbers")
	public List<Integer> getNumbers() {
		loading();
		return initArrs();
	}

	@GetMapping("/numbers/random")
	public Integer getRandomOrg() {
		loading();
		return getRandom();
	}

	private void loading() {
		try {
			TimeUnit.MILLISECONDS.sleep(new Random().nextInt(3000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private List<Integer> initArrs() {
		Random random = new Random();
		int size = random.nextInt(90) + 10;
		List<Integer> result = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			result.add(random.nextInt(100) + 1);
		}
		return result;
	}

	private Integer getRandom() {
		Random random = new Random();
		return random.nextInt(100) + 1;
	}
}
