package com.example.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class TestApplciation {

	private static final String URL = "http://localhost:8080/api/numbers";
	private static RestTemplate restTemplate = new RestTemplate();

	public static void main(String[] args) {
		ex5();
	}

	static void ex1() {
		ExecutorService executor = Executors.newFixedThreadPool(5);

		List<CompletableFuture<Integer>> futures = new ArrayList<>();

		CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
			List<Integer> list = (List<Integer>) getObj(URL);
			System.out.println(Thread.currentThread().getName() + " - Value: " + list);
			return list;
		}, executor).thenApplyAsync(res -> {
			for (Integer item : res) {
				CompletableFuture<Integer> futureItem = CompletableFuture.supplyAsync(() -> {
					Integer value = (Integer) getObj(URL + "/" + item);
					System.out.println(Thread.currentThread().getName() + " - Value: " + value);
					return value;
				}, executor);
				futures.add(futureItem);
			}
			return res.size();
		});

		System.out.println("DONE");

//		executor.shutdown();
//		while (!executor.isTerminated()) {
//
//		}
	}

	// Tính tổng của 4 Future dùng combine, mặc định Thread quản lý bằng
	// ForkJoinPool
	static void ex2() {
		CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> {
			Integer value = (Integer) getObj(URL + "/random");
			System.out.println("1: " + Thread.currentThread().getName() + " - Value: " + value);
			return value;
		});
		CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> {
			Integer value = (Integer) getObj(URL + "/random");
			System.out.println("2: " + Thread.currentThread().getName() + " - Value: " + value);
			return value;
		});
		CompletableFuture<Integer> future3 = CompletableFuture.supplyAsync(() -> {
			Integer value = (Integer) getObj(URL + "/random");
			System.out.println("3: " + Thread.currentThread().getName() + " - Value: " + value);
			return value;
		});
		CompletableFuture<Integer> future4 = CompletableFuture.supplyAsync(() -> {
			Integer value = (Integer) getObj(URL + "/random");
			System.out.println("4: " + Thread.currentThread().getName() + " - Value: " + value);
			return value;
		});

		CompletableFuture<Integer> result = future1
				.thenCombine(future1, (a, b) -> a + b)
				.thenCombine(future2, (a, b) -> a + b)
				.thenCombine(future3, (a, b) -> a + b)
				.thenCombine(future4, (a, b) -> a + b);

		System.out.println("DONE");

		try {
			System.out.println("SUM :" + result.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	static void ex3() {
		ExecutorService executor = Executors.newFixedThreadPool(5);
		List<CompletableFuture<Integer>> futures = new ArrayList<>();
		for (int i = 0; i < 20; i++) {
			CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
				Integer value = (Integer) getObj(URL + "/random");
				System.out.println(Thread.currentThread().getName() + " - Value: " + value);
				return value;
			}, executor);
			futures.add(future);
		}

		CompletableFuture<Void> result = CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]))
				.thenRunAsync(() -> {
					System.out.println("SIZE: " + futures.size() + " - " + Thread.currentThread().getName());
					for (CompletableFuture<Integer> future : futures) {
						System.out.println(future.isDone());
					}
				}, executor);

		System.out.println("DONE");

//		executor.shutdown();
//		while (!executor.isTerminated()) {
//
//		}
	}

	static void ex4() {
		ExecutorService executor = Executors.newFixedThreadPool(5);
		List<CompletableFuture<Integer>> futures = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
				Integer value = (Integer) getObj(URL + "/random");
				System.out.println(Thread.currentThread().getName() + " - Value: " + value);
				return value;
			}, executor);
			futures.add(future);
		}

		CompletableFuture<Object> result = CompletableFuture
				.anyOf(futures.toArray(new CompletableFuture[futures.size()]));

		System.out.println("DONE");

		try {
			System.out.println(result.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		executor.shutdown();
		while (!executor.isTerminated()) {

		}
	}

	static void ex5() {

		CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
			Integer value = (Integer) getObj(URL + "/random");
			System.out.println(Thread.currentThread().getName() + " - Value: " + value);
			return value;
		}).thenCompose(res -> {
			return CompletableFuture.supplyAsync(() -> {
				Integer value = (Integer) getObj(URL + "/" + res);
				System.out.println(Thread.currentThread().getName() + " - Value: " + value);
				return value;
			});
		}).exceptionally(ex -> {
			ex.printStackTrace();
			return -1;
		});

		try {
			System.out.println(future.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	private static Object getObj(String url) {
		ResponseEntity<Object> responseEntity = restTemplate.getForEntity(url, Object.class);
		if (responseEntity.hasBody()) {
			return responseEntity.getBody();
		}
		return null;
	}
}
